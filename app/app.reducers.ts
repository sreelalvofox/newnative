import { ActionReducerMap } from "@ngrx/store";
import { RouterReducerState, routerReducer } from "@ngrx/router-store";

import * as confState from "./passit-frontend/get-conf/conf.reducer";
import * as contactsState from "./passit-frontend/group/contacts/contacts.reducer";
import * as secretState from "./passit-frontend/secrets/secrets.reducer";

export interface IState {
    contacts: contactsState.IContactsState;
    router: RouterReducerState;
    secrets: secretState.ISecretState;
    conf: confState.IConfState;
}

export const reducers: ActionReducerMap<IState> = {
    contacts: contactsState.contactsReducer,
    secrets: secretState.secretReducer,
    router: routerReducer,
    conf: confState.reducer,
};
