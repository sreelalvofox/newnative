import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from '@ngrx/store';

import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule} from "nativescript-angular/forms"

import { SecretListContainer } from "../passit-frontend/list/list.container";
import { SecretListComponent } from "./list.component";
import { SecretFormContainer } from "../passit-frontend/list/secret-form/secret-form.container";
import { SecretFormComponent } from "./secret-form/secret-form.component";
import { SecretDetailComponent } from "./detail.component";
import { reducers } from "../passit-frontend/list/list.reducer";
import { MobileMenuModule } from "../mobile-menu"
import { ListActionBarContainer, ListActionBarComponent } from "./actionbar";
import { SecretFormEffects } from "../passit-frontend/list/secret-form/secret-form.effects";
import { NgrxFormsModule } from "ngrx-forms";
import { DirectivesModule } from "~/directives";
import { NSSecretFormEffects } from "./secret-form/secret-form.effects";
import { SecretNewComponent } from "~/list/new.component";

export const COMPONENTS = [
  SecretListComponent,
  SecretListContainer,
  SecretFormComponent,
  SecretFormContainer,
  SecretNewComponent,
  SecretDetailComponent,
  ListActionBarContainer,
  ListActionBarComponent,
];

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    MobileMenuModule,
    NgrxFormsModule,
    DirectivesModule,
    StoreModule.forFeature('list', reducers),
    EffectsModule.forFeature([SecretFormEffects, NSSecretFormEffects])
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [],
	schemas: [NO_ERRORS_SCHEMA]
})
export class ListModule { }
