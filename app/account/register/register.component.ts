import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  ElementRef
} from "@angular/core";
import { IRegisterForm, IUrlForm } from "~/passit-frontend/account/register/interfaces";
import { FormGroupState, MarkAsSubmittedAction } from "ngrx-forms";
import { RegisterStages } from "~/passit-frontend/account/constants";
import { ActionsSubject } from "@ngrx/store";

@Component({
  selector: "register-component",
  moduleId: module.id,
  templateUrl: "./register.component.html",
})
export class RegisterComponent {
  @Input() errorMessage: string;
  @Input() form: FormGroupState<IRegisterForm>;
  @Input() urlForm: FormGroupState<IUrlForm>;
  @Input() isEmailTaken: boolean;
  @Input() isUrlValid: boolean | undefined;
  @Input() showUrl: string;
  @Input() isExtension: boolean;
  @Input() urlDisplayName: string;
  @Input() hasSubmitStarted: boolean;
  @Input() hasSubmitFinished: boolean;

  @Output() register = new EventEmitter();
  @Output() goToLogin = new EventEmitter<string>();
  @Output() checkEmail = new EventEmitter();
  @Output() checkUrl = new EventEmitter<string>();
  @Output() toggleShowConfirm = new EventEmitter();
  @Output() markAsSubmitted = new EventEmitter();
  @Output() incrementStage = new EventEmitter();
  @Output() switchStage = new EventEmitter<number>();
  @Output() displayUrlInput = new EventEmitter();
  @Output() hideUrlInput = new EventEmitter();
  @Output() setNewsletterSubscribe = new EventEmitter();
  @Output() registrationFinished = new EventEmitter();

  _stageValue: number;
  passwordFocused = false;
  checked: boolean;
  stages = RegisterStages;

  @ViewChild("passwordInput") passwordInput: ElementRef;

  @ViewChild("newsletterInput") newsletterInput: ElementRef;

  constructor(private actionsSubject: ActionsSubject) {}

  @Input()
  set stageValue(value: RegisterStages) {
    this._stageValue = value;
    switch (value) {
      case this.stages.Password:
        setTimeout(() => this.passwordInput.nativeElement.focus(), 0);
        return;
      case this.stages.Newsletter:
        setTimeout(() => this.newsletterInput.nativeElement.focus(), 0);
        return;
    }
  }

  get stageValue(): RegisterStages {
    return this._stageValue;
  }

  submit() {
    switch (this.stageValue) {
      case this.stages.Email:
        this.submitEmail();
        return;

      case this.stages.Password:
        if (!this.form.errors._password && !this.form.errors._passwordConfirm) {
          this.incrementStage.emit();
        } else if (this.form.isUnsubmitted) {
          this.actionsSubject.next(new MarkAsSubmittedAction(this.form.id));
        }
        return;

      case this.stages.Newsletter:
        this.setNewsletterSubscribe.emit();
        this.submitForm();
        return;

      case this.stages.Verified:
        this.registrationFinished.emit();
        return;
    }
  }

  submitEmail() {
    if (this.form.controls.email.isValid) {
      this.checkEmail.emit();
    } else if (this.form.isUnsubmitted) {
      this.actionsSubject.next(new MarkAsSubmittedAction(this.form.id));
    }
  }

  urlSubmit() {
    if (this.urlForm.isValid) {
      this.checkUrl.emit();
    } else {
      this.actionsSubject.next(new MarkAsSubmittedAction(this.urlForm.id));
    }
  }

  switchCurrentStage(value: number) {
    this.switchStage.emit(value);
  }

  displayUrl() {
    this.displayUrlInput.emit();
  }

  hideUrl() {
    this.hideUrlInput.emit();
  }

  submitForm() {
    if (this.form.isValid) {
      this.register.emit();
    } else {
      this.markAsSubmitted.emit();
    }
  }

  toggleDisplay() {
    this.goToLogin.emit();
  }

  goToLoginWith() {
    this.goToLogin.emit(this.form.value.email);
  }
}

